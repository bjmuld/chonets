import numpy as np
import networkx as nx
import matplotlib.pyplot as plt
import sympy.functions.special.polynomials as polynomials
import sympy as sp
import symengine as se
import xanity

from collections import OrderedDict
from sympy.abc import x
import jitcode
from scipy.spatial import distance


class System(nx.DiGraph):

    def __init__(self, graph, linearity, order=3, bases='legendre', symbolic_type=jitcode.y):
        # map to DiGraph:
        super(System, self).__init__(graph)
        self.linearity = linearity
        self.poly_order = order
        self.ode_obj = None
        self.system_expression = None
        if isinstance(graph, nx.MultiDiGraph):
            for edge in graph.edges():
                self.add_edge(*edge, coefs=None)
                self.randomize_edge_coefs(linearity, edge=edge)
            for node in self.nodes():
                self.node[node]['mixing_matrix'] = None
                self.node[node]['mixing_mask'] = None
                self.randomize_node_mixing_matrix(linearity, node=node)

        elif isinstance(graph, System):
            for edge in graph.edges():
                if 'coefs' in graph.edges[edge]:
                    self.add_edge(*edge, coefs=graph.edges[edge]['coefs'])
                else:
                    self.add_edge(*edge, coefs=None)
                    self.randomize_edge_coefs(linearity, edge=edge)

            for node in self.nodes():
                if 'mixing_matrix' in graph.node[node]:
                    self.node[node]['mixing_matrix'] = graph.node[node]['mixing_matrix']
                    self.node[node]['mixing_mask'] = graph.node[node]['mixing_mask']
                else:
                    self.node[node]['mixing_matrix'] = None
                    self.node[node]['mixing_mask'] = None
                    self.randomize_node_mixing_matrix(linearity, node=node)

                if 'state' in graph.node[node]:
                    self.node[node]['state'] = graph.node[node]['state']
                else:
                    self.node[node]['state'] = 0.0

        if isinstance(bases, list) and ('symengine' in str(bases[1].__class__) or 'sympy' in str(bases[1].__class__)):
            self.bases = bases

        elif isinstance(bases, str):
            try:
                bases = getattr(polynomials, bases)
                self.bases = [se.sympify(bases(ord, x)) for ord in range(order + 1)]

            except Exception as e:
                print('bad specification of bases in call to System.__init__()')
        else:
            print('bad specification of bases in call to System.__init__()')

        if isinstance(symbolic_type, (list, tuple)):
            self.symbolic_vector = symbolic_type
        else:
            try:
                self.symbolic_vector = [symbolic_type(node) for node in self.nodes]
            except TypeError:
                self.symbolic_vector = [symbolic_type('x'+str(node)) for node in self.nodes]

        self.resolve_system_expression()

    def randomize_edge_coefs(self, linearity, edge=None, dc=False):
        def do_edge(edge):
            if dc:
                dc_coef = np.random.uniform(-1, 1)
            else:
                dc_coef = 0
            lin_coef = np.random.uniform(-1, 1)
            rem_energy = abs(lin_coef) * (1.0 - linearity)
            rem_coefs = rem_energy * np.random.uniform(-1, 1, (self.poly_order-1, ))
            coefs = np.r_[dc_coef, lin_coef, rem_coefs]
            if np.sum(abs(coefs)) > 1.0:
                coefs = np.random.uniform(0, 1) * coefs / np.sum(abs(coefs))
            self.edges[edge]['coefs'] = coefs

        if edge is None:
            # do all edges
            for edge in self.edges():
                do_edge(edge)
        else:
            do_edge(edge)

    def randomize_node_mixing_matrix(self, linearity, node=None, dc=False):
        def do_node(self, node):
            if self.in_degree(node) > 0:
                mask = np.flipud(np.tri(self.poly_order+1)) * np.tri(self.poly_order+1)
                if not dc:
                    mask[0, 0] = 0
                mask = np.repeat(mask, self.in_degree(node), axis=0)
                mask = np.repeat(mask, self.in_degree(node), axis=1)
                mask = np.vstack([np.zeros((1, self.in_degree(node)*(self.poly_order+1))), mask])
                mask = np.hstack([mask, np.zeros((self.in_degree(node)*(self.poly_order+1)+1, 1))])
                np.fill_diagonal(mask, 0.0)
                mask = np.tril(mask)

                lin_coefs = np.random.uniform(-1, 1, (mask.shape[0], 1))
                rem_energy = (1-linearity) * np.sum(abs(lin_coefs))
                rem_coefs = rem_energy * np.random.uniform(-1, 1, (mask.shape[0], mask.shape[1]-1))
                coefs = np.c_[lin_coefs, rem_coefs]
                coefs = mask * coefs
                tot_energ = np.sum(abs(coefs))
                if tot_energ > 1.0:
                    coefs = np.random.uniform(0, 1) * coefs/tot_energ

                self.node[node]['mixing_mask'] = mask
                self.node[node]['mixing_matrix'] = coefs
            else:
                self.node[node]['mixing_mask'] = np.zeros((1, 1))
                self.node[node]['mixing_matrix'] = np.zeros((1, 1))

        if node is None:
            # do all nodes:
            for node in self.nodes():
                do_node(self, node)
        else:
            do_node(self, node)

    def _calc_system_expression(self, symbolic_vector):
        return OrderedDict({
            symbolic_vector[node]: self.get_node_expression(node, symbolic_vector) for node in self.nodes
        })

    def resolve_system_expression(self):
        self.system_expression = self._calc_system_expression(self.symbolic_vector)

    def get_node_expression(self, node, symbolic_vector):
        edges = self.in_edges(node)
        if len(edges) == 0:
            expr = 0.0

        else:
            in_sigs = []

            for edge in edges:
                expr = []
                for ord, coef in enumerate(self.edges[edge]['coefs']):
                    expr.append(coef * self.bases[ord].subs({x: symbolic_vector[edge[0]]}))
                in_sigs.append(expr)

            in_sigs = sp.Matrix(np.vstack([1, np.array(in_sigs).T.reshape(-1, 1)]))
            in_sigs = in_sigs * in_sigs.T
            mm = self.nodes[node]['mixing_matrix']
            expr = np.sum(np.multiply(in_sigs, mm))

        if 'symengine' in str(symbolic_vector[0].__class__):
            return se.sympify(expr)
        elif 'sympy' in str(symbolic_vector[0].__class__):
            return sp.sympify(expr)

    def reset(self, initial_conditions=None):
        for node in self.nodes():
            if initial_conditions is not None:
                self.node[node]['state'] = initial_conditions[node]
            else:
                self.node[node]['state'] = 0.0

    # def step(self, epsilon):
    #     new_states = []
    #
    #     for node in self.nodes:
    #
    #         int_sigs = [1]
    #         for coef_no in range(1, 1+self.graph['order']):
    #             for edge in self.in_edges(node):
    #                 if self.edges[edge]['coefs'][coef_no] != 0.0:
    #                     origin_val = self.nodes('state')[edge[0]]
    #                     poly_c = np.r_[np.zeros(coef_no), self.edges[edge]['coefs'][coef_no]]
    #                     int_sigs.append(np.polynomial.chebyshev.Chebyshev(poly_c)(origin_val))
    #                 else:
    #                     int_sigs.append(0.0)
    #
    #         if len(int_sigs) == 1:
    #             new_states.append(old_state)
    #             continue
    #
    #         int_sigs = np.matrix(int_sigs)
    #         int_sigs = int_sigs.T * int_sigs
    #
    #         old_state = self.node[node]['state']
    #         mix_mat = self.nodes('mixing_matrix')[node]
    #         forces = np.array(int_sigs) * mix_mat
    #
    #         new_state = old_state + np.sum(forces) * epsilon
    #         new_state = np.clip(new_state, -1.0, 1.0)
    #         new_states.append(new_state)
    #
    #     for i, node in enumerate(self.nodes):
    #         self.nodes[node]['state'] = new_states[i]
    #
    #     return

    def perturb_params(self, magnitude):
        G = System(self, linearity=self.linearity, order=self.poly_order, bases=self.bases,
                   symbolic_type=self.symbolic_vector)

        for node in self.nodes():
            old_mat = self.nodes[node]['mixing_matrix']
            if old_mat is None:
                continue
            mask = self.nodes[node]['mixing_mask']
            scale = old_mat / np.max(np.abs(old_mat))
            new_mat = old_mat + mask * magnitude * scale * np.random.randn(*old_mat.shape)
            tot_energ = np.sum(abs(new_mat))
            if tot_energ > 1.0:
                new_mat = new_mat / tot_energ
            G.nodes[node]['mixing_matrix'] = new_mat

        for edge in self.edges():
            old_coefs = self.edges[edge]['coefs']
            scale = old_coefs / np.max(np.abs(old_coefs))
            new_coefs = old_coefs + magnitude * scale * np.random.randn(*old_coefs.shape)
            tot_energ = np.sum(abs(new_coefs))
            if tot_energ > 1:
                new_coefs = new_coefs/tot_energ
            G.edges[edge]['coefs'] = new_coefs

        G.resolve_system_expression()
        return G

    @property
    def state(self):
        states = self.nodes.data('state')
        result = []
        for node in self.nodes:
            result.append(states[node])
        return np.array(result)

    def simulate(self, steps, stepsize):
        time_vec = np.linspace(0, steps*stepsize, steps)

        self.ode_obj = jitcode.jitcode(self.system_expression)
        self.ode_obj.set_integrator('dopri5')
        self.ode_obj.set_initial_value(self.state, 0.0)
        data = []

        for time in time_vec:
            state = self.ode_obj.integrate(time)
            data.append(state)

        return np.array(data)

    def simulate_lyap(self, steps, stepsize):
        time_vec = np.linspace(0, steps * stepsize, steps)

        self.ode_obj = jitcode.jitcode_lyap(self.system_expression, n_lyap=1, simplify=True)
        self.ode_obj.set_integrator('dopri5')
        self.ode_obj.set_initial_value(self.state, 0.0)
        data = []
        les = []
        lvs = []

        for time in time_vec:
            state, le, lv = self.ode_obj.integrate(time)
            data.append(state)
            les.append(le)
            lvs.append(lv)

        return np.array(data), np.array(les), np.array(lvs)

    # def simulate(self, steps, stepsize, plot=False):
    #     record = np.zeros((steps, self.state.size))
    #
    #     for i in range(steps):
    #         self.step(stepsize)
    #         record[i, :] = self.state
    #         if plot:
    #             if 'fig' not in locals():
    #                 fig = plt.figure()
    #                 ax = fig.add_subplot(111)
    #                 lines = ax.plot(record)
    #                 plt.show(block=False)
    #             else:
    #                 for sno, line in enumerate(lines):
    #                     line.set_ydata(record[:, sno])
    #                 fig.canvas.draw()
    #                 fig.canvas.flush_events()
    #                 time.sleep(1/24)
    #
    #     fig.close()
    #     return record

    def test_for_ic_chaos(self, trials, steps_per_trial, step_size, delta_ic_mag, threshold=0.01, plot=False):
        ic_chaos = np.zeros(trials)

        # create base and perturbed ICs
        initial_conditions = np.random.uniform(-1, 1, (trials, len(self.nodes)))
        modified_ics = initial_conditions + delta_ic_mag * np.random.uniform(-1, 1, initial_conditions.shape)
        if np.any(abs(modified_ics) > 1.0):
            modified_ics = modified_ics / np.max(abs(modified_ics))

        for trial in range(trials):
            xanity.log('starting ic trial {}'.format(trial))
            self.reset(initial_conditions[trial])
            base_trajectory, les, lvs = self.simulate_lyap(steps_per_trial, step_size)

            # self.reset(modified_ics[trial])
            # delta_trajectory = self.simulate(steps_per_trial, step_size)
            #
            # delta_0 = distance.euclidean(initial_conditions[trial], modified_ics[trial])
            #
            # delta_ic_distance = np.array([
            #     distance.euclidean(base_trajectory[i, :], delta_trajectory[i, :]) for i in range(steps_per_trial)
            # ])
            #
            # ic_lyapunov = np.log(delta_ic_distance/delta_0) / np.arange(len(delta_ic_distance))

            if plot:
                f = plt.figure()
                axs = f.subplots(3, 1)
                axs[0].plot(base_trajectory); axs[0].set_title('Trajectories')
                axs[1].plot(les); axs[1].set_title('Lyapunov Exponents')
                axs[2].plot(np.squeeze(lvs)); axs[2].set_title('Lyapunov Vectors')
                plt.tight_layout()
                plt.show()

            if np.mean(les[np.logical_not(np.isnan(les))]) > threshold:
                ic_chaos[trial] = 2
                return ic_chaos
            else:
                ic_chaos[trial] = 1

        return ic_chaos

    def test_for_theta_chaos(self, trials, steps_per_trial, step_size, delta_theta_mag, threshold=0.03, plot=False):
        theta_chaos = np.zeros(trials)

        # create ICs
        initial_conditions = np.random.uniform(-1, 1, (trials, len(self.nodes)))

        # create parametrically perturbed system
        mod_graph = self.perturb_params(delta_theta_mag)

        for trial in range(trials):
            xanity.log('starting tc trial {}'.format(trial))
            self.reset(initial_conditions[trial])
            mod_graph.reset(initial_conditions[trial])

            base_trajectory = self.simulate(steps_per_trial, step_size)
            perturb_param_trajectory = mod_graph.simulate(steps_per_trial, step_size)

            Z_0 = distance.euclidean(base_trajectory[1], perturb_param_trajectory[1])

            delta_theta_distance = np.array([
                distance.euclidean(base_trajectory[i, :], perturb_param_trajectory[i, :]) for i in range(steps_per_trial)
            ])

            theta_lyapunov = np.log(delta_theta_distance/Z_0) / np.arange(len(delta_theta_distance))

            if plot:
                f = plt.figure()
                axs = f.subplots(2, 1)
                axs[0].plot(base_trajectory)
                axs[0].plot(perturb_param_trajectory)
                axs[0].set_title('Trajectories')
                axs[1].plot(theta_lyapunov)
                axs[1].set_title('Lyapunov Exponents')
                plt.tight_layout()
                plt.show()

            if np.mean(theta_lyapunov[
                           np.logical_not(np.logical_or(np.isnan(theta_lyapunov), np.isinf(theta_lyapunov)))
                       ]) > threshold:
                theta_chaos[trial] = 2
                return theta_chaos
            else:
                theta_chaos[trial] = 1

        return theta_chaos

    def nullclines(self):

        symb_vect = [sp.Symbol("x{}".format(ind)) for ind in self.nodes]
        expr = self._calc_system_expression(symb_vect)
        soln = sp.solve(list(expr.values()), list(expr.keys()), dict=True)[0]

        nc = {k: soln[k] if k in soln.keys() else sp.Interval(-sp.oo, sp.oo)
                for k in expr.keys()}

        fp = sp.solve_poly_system(soln, list(expr.keys()))

    def nc2fp(self, nullclines):
        sys = {k: v for k, v in nullclines.items() if not isinstance(v, sp.Interval)}
        soln = solve(list(nullclines.values()), list(nullclines.keys()))[0]
        return {k: soln[k] for k in nullclines.keys() if k in soln.keys()}

    def fixed_points(self):
        nc = self.nullclines()
        return self.nc2fp(nc)