import xanity
import numpy as np
import networkx as nx
from matplotlib import pyplot as plt
import scipy.spatial.distance as distance

from sympy import Symbol
from common.classes import System

xanity.experiment_parameters({
    'linearity': np.linspace(0.5, 1-1e-3, 5),
    'dimensionality': np.arange(3, 100, 10),
    'order': np.arange(3, 8)
})


def main(
        n_graphs=100,
        linearity=1.0,
        order=5,
        dimensionality=10,
        connectivity='scale_free_graph',
        trials=2,
        t_max=500.0,
        n_samples=1000,
        ic_perturb_scale=1e-5,
        param_perturb_scale=1e-5,
        plot=False):

    systems = []
    ic_chaos = []
    theta_chaos = []

    step_size = t_max/(n_samples-1)
    steps_per_trial = n_samples

    for i in range(n_graphs):

        xanity.log('starting graph {}'.format(i))
        # create graphs for analysis:
        s = eval('nx.' + connectivity)(dimensionality)

        # convert the graph to a system:
        s = System(s, linearity=linearity, order=order, bases='legendre')
        systems.append(s)

        nc = s.nullclines()
        fp = s.nc2fp(nc)

        print('null_clines:\n{}'.format(''.join(['\n    {}:{}'.format(k, v) for k, v in nc.items()])))
        ic = s.test_for_ic_chaos(trials, steps_per_trial, step_size, ic_perturb_scale, plot=plot)
        ic_chaos.append(True if np.any(ic == 2) else False)

        tc = s.test_for_theta_chaos(trials, steps_per_trial, step_size, param_perturb_scale, plot=plot)
        theta_chaos.append(True if np.any(tc == 2) else False)


if __name__ == '__main__':
    xanity.run()
